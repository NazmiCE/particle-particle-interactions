import math 

kb = 1.38 * 10**(-23) # Boltzmann Constant
T = 310 # K body temperature
n = 7.2*10**(-4) # Pa*s viscosity of water
I = kb*T/(6*math.pi*n) # D = I/a