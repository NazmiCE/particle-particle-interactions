class Region:
    def __init__(self, x = 250, y = 200, z = 10**(-8), flow = 0) -> None:
        """
        x unit in mikron\n
        y unit in mikron\n
        z unit in mikron\n
        """
        self.x = x # mikron
        self.y = y # mikron
        self.name = f"Region {self.x}x{self.y}"
        self.z = z # mikron diameter of particle
        self.flow = flow

    def __str__(self) -> str:
        return f"Region {self.x}x{self.y}"
    
    def region_volume(self) -> float:
        """
        Returns the volume of the region in cm^3. 
        """
        return self.x*self.y*self.z*10**(-12)
    
    def set_flow(self) -> None: # No implementation
        pass
