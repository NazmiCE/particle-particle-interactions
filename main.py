import region
from constants import *
import numpy as np
import scipy.special
import scipy.spatial
import math

def main():
    diameter = 0.15 # Nanoparticle diameter in mikron
    x = 250 # x Dimension of the Region
    y = 200 # y Dimension of the Region
    D = 2*I/(diameter*10**(-6)) # Diffusibility of the Nanoparticle Concentration
    


    model_region = region.Region(x, y, diameter)

    initial = 3*10**(8) # The initial data all the other samples will be created based on this
    samples = []
    for i in range(1,10):
        samples.append(initial*1.5**(i))

    samples_np = np.array(samples)
    volume = model_region.region_volume()
    number_of_particles = np.round(volume * samples_np)
    
    print(f"Experiment Volume: {volume} cubicmikron")
    final_samples = samples_np[number_of_particles >= 2]
    final_nop = number_of_particles[number_of_particles >= 2]
    print("The Experiment Samples")
    print(final_samples)
    print(final_nop)
    print("*************************************************************")
    print("Start of Experiment:")

    rng = np.random.default_rng()
    final_data = []
    for i,j in zip(final_nop, final_samples):
        ind_data = []
        count = 0
        print("******************************")
        print(i, j)
        for iteration in range(5):
            t_req = 10000# Seconds
            positions = rng.integers((0,0),(x,y), size=(int(i),2)) # x-y region in mikrons for both dimension
            closest_particle_distance = 10**(5) # Arbitrary distance in mikron
            for k in range(int(i)):
                a = np.delete(positions, k, axis=0)
                T = scipy.spatial.KDTree(a)
                idx = T.query(positions[k])
                if idx[0] < closest_particle_distance and idx[0] != 0:
                    closest_particle_distance = idx[0]
                #elif idx[0] == 0:
                #    print(a[idx[1]], positions[k])
                #    closest_particle_distance = 10**(-3) # resulted exagerated phenonema
                if closest_particle_distance < 1:
                    break
            
            while t_req < 100_000:  # Approximately 2 month  
                inside = ((closest_particle_distance - (diameter/1000))*10**(-6))/(math.sqrt(4*D*t_req)) 
                if closest_particle_distance == 0.0:
                    #print(positions)
                    quit()
                else:
                    pre_term = (diameter/1000)/closest_particle_distance 
                pcol = pre_term*scipy.special.erfc(inside)
                print(pcol)
                if pcol > 0.05:
                    count += 1
                    
                    print(i, t_req)
                    print(closest_particle_distance)
                    ind_data.append(t_req)
                    break
                t_req += 10000
                    

        final_data.append(ind_data)     

def deneme():
    x, y = np.mgrid[0:5, 0:5]
    tree = scipy.spatial.KDTree(np.c_[x.ravel(), y.ravel()])
    print(np.c_[x.ravel(), y.ravel()])
    dd, ii = tree.query([[1, 3.5]], k=1)
    print(dd, ii, sep='\n')

if __name__ == "__main__":
    main()
    #deneme()